# Paperless Permission Slips User Guide
Kia ora and welcome to Paperless Permission Slips User Guide. This guide will help you learn how to send a permission slip, and see the results. Let's get started.
## Summary
There are 3 parts to the system:
1.  Creating a Google Form (this is the permission slip)
2.  Sending the email containing the form out to a segment of the school.
3.  Reviewing the results of the email

## Creating a Google Form
### Accessing Google Forms
Log into Google Apps (GSuite) using your school Google account.

Click Google Apps menu top right (it looks like a 3x3 grid of dots). Then click on More.
![Image of Apps Menu](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/Appsmenu.png "Apps Menu")

Then click on Forms.

![Image of Forms on Apps Menu](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/FormsOnAppsMenu.png "Forms on Apps Menu")

That should land you in Google Forms.


### Creating a new Permission slip Form
Click on "Master Template" to open it.

![Image of Master Template](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/MasterForm.png "Master Template")

**Important** Then at the top right, click the "3 dots" icon, and click on "Make a copy". It is important to do this as we do *not* want to edit or send out the Master Template.

![Image of Make a Copy link](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/MakeACopy.png "Make a Copy link")

Give your copy a meaningful name, so that in future others may choose to copy it if they know it is a close fit for their new permission slip.
**Remember** to tick "Share it with the same people".

![Image of Copy naming dialog](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/CopyRename.png "Name your Copy")

And then your new permission slip is open for editing.

### Edit your Permission slip

We have put a selection of common questions into the Master Template.

Fill in the blanks and / or replace the placeholders. 

![Image of Replaced Placeholder](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/ReplacePlaceholder.png "Replace Placeholders")

Delete the questions that are not relevant, or edit them to be relevant.

![Image of Delete Field Icon](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/DeleteFields.png "Delete Irrelevant Fields")

Add new questions, sections etc using the standard Forms features. Make fields mandatory. Add images, sections, videos etc.

![Image of Add Field Icons](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/AddFields.png "Add Fields")

When you think you are done, preview and test your form using the Preview feature.

![Image of Preview Icon](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/PreviewForm.png "Preview Form")

**TIP** We think every form should always ask for the parent / caregiver email address, parent / caregiver name, and pupil name. This is to help matching response to pupil.


When you are happy with your form, take a moment to enjoy your success, then proceed to the next section.

## Sending the email containing the form out to a segment of the school
### Copy Form Link and Response spreadsheet link from Google Forms
Once the new form/permission is complete , click on Send button at the top right of the page

Select the Link icon next to Send via and a link to the form will be shown as below

![Image of Copy Link](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/CopyLink.png "Copy Link")

Select Shorten URL if you want to use a shortened version of the URL

![Image of Shorten URL](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/ShortenURL.png "Shorten URL")

Click on Copy and save this link on a notepad for later use

Navigate to Responses and ensure the Accepting responses is selected (move the bar to extreme right).

Click on the icon in green to create spreadsheet.

![Image of Responses Page](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/ResponsesPage.png "Responses Page")

![Image of Create Response Spreadsheet](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/CreateResponseSpreadsheet.png "Create Response Spreadsheet")

Choose the option Create a new spreadsheet and click on Create button

Copy the link in the browser window and save this link on a notepad for later use

![Image of Spreadsheet Link](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/SpreadsheetLink.png "Spreadsheet Link")

### Accessing the new Paperless system

Log into the new Paperless system using the URL - Add URL here
1.  Add the Event Name
2.  Paste the copied Form link into the Form link field
3.  Paste the copied Spreadsheet link into the Result Sheet link
4.  Add details for the email : Subject, Email Body
5.  Select the segment the email should be sent to
6.  Click on Create Event

### To access Google Forms results.  (the completed permission slips)

Log back in to Google Forms, and click on the Form whose data you wish to review.

Click on the Responses tab, then the green icon again. This time you will be taken to Google Sheets, and a spreadsheet of all data from the form is available for you to work with.

## For more help

[Google Forms user guide](https://gsuite.google.com/learning-center/products/forms/get-started/)

Is the system not working? Contact Adrian on 021 914270 or email him on adrian@iceknife.com




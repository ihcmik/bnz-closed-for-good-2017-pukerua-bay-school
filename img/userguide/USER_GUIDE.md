# Paperless Permission Slips User Guide
Kia ora and welcome to Paperless Permission Slips User Guide. This guide will help you learn how to send a permission slip, and see the results. Let's get started.
## Summary
There are 3 parts to the system:
1.  Creating a Google Form (this is the permission slip)
2.  Sending the email containing the form out to a segment of the school.
3.  Reviewing the results of the email

## Creating a Google Form
### Accessing Google Forms
Log into Google Apps (GSuite) using your school Google account.

Click Google Apps menu top right (it looks like a 3x3 grid of dots). Then click on More.
![Image of Apps Menu](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/Appsmenu.png "Apps Menu")

Then click on Forms.

![Image of Forms on Apps Menu](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/FormsOnAppsMenu.png "Forms on Apps Menu")

That should land you in Google Forms.


### Creating a new Permission slip Form
Click on "Master Form" to open it.

![Image of Master Form](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/MasterForm.png "Master Form")

**Important** Then at the top right, click the "3 dots" icon, and click on "Make a copy". It is important to do this as we do not want to edit or send out the Master Form, as this would change it for everyone making subsequent forms harder to compose.

![Image of Make a Copy link](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/MakeACopy.png "Make a Copy link")


## Sending the email containing the form out to a segment of the school
### Copy Form Link and Response spreadsheet link from Google Forms
Once the new form/permission is complete , click on Send button at the top right of the page

Select the Link icon next to Send via and a link to the form will be shown as below

![Image of Copy Link](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/CopyLink.png "Copy Link")

Select Shorten URL if you want to use a shortened version of the URL

![Image of Shorten URL](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/ShortenURL.png "Shorten URL")

Click on Copy and save this link on a notepad for later use

Navigate to Responses and ensure the Accepting responses is selected (move the bar to extreme right)

![Image of Responses Page](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/ResponsesPage.png "Responses Page")

Click on the icon in green to create spreadsheet

![Image of Create Response Spreadsheet](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/CreateResponseSpreadsheet.png "Create Response Spreadsheet")

Choose the option Create a new spreadsheet and click on Create button

Copy the link in the browser window and save this link on a notepad for later use

![Image of Spreadsheet Link](https://bitbucket.org/ihcmik/bnz-closed-for-good-2017-pukerua-bay-school/raw/master/img/userguide/SpreadsheetLink.png "Spreadsheet Link")


### Accessing the new Paperless system
Log into the new Paperless system using the URL - Add URL here
1.  Add the Event Name
2.  Paste the copied Form link into the Form link field
3.  Paste the copied Spreadsheet link into the Result Sheet link
4.  Add details for the email : Subject, Email Body
5.  Select the segment the email should be sent to
6.  Click on Create Event